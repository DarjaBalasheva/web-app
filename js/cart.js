let tg = window.Telegram.WebApp;

tg.expand();
tg.MainButton.show();
tg.MainButton.text = "Отправить";
tg.BackButton.show();

document.addEventListener('DOMContentLoaded', function () {

    const cartList = document.getElementById('productList');
    const sendButton = document.getElementById('sendButton'); // Получаем ссылку на кнопку "Отправить"
    const jsonData = document.getElementById('jsonData');

    // Получите объект cart из localStorage
    let cart = JSON.parse(localStorage.getItem('cart')) || [];
    let cartToSend = localStorage.getItem('cartToSend')||[];
    let order = localStorage.getItem('order') || 0;
    let min_order = localStorage.getItem('min_order') || 0;
    console.log(min_order);

    function displayCart() {
    // Очистите содержимое корзины, чтобы избежать дублирования
    cartList.innerHTML = '';

    // Отобразите товары и их количество из объекта cart
    cart.forEach(item => {
        if (item.count > 0) { // Отображаем только товары с количеством больше нуля
            const cartItem = document.createElement('div');
            cartItem.className = "product";
            cartItem.style.borderBottom = "var(--bottom-lineary)";
            cartItem.innerHTML = `
                <h2 class="product-name-cart">${item.name_nom}</h2>
                <h2 class="product-count-cart">${item.count}</h2>
            `;
            cartList.appendChild(cartItem);
            order += item.count * item.price
        }

    });
    }

    function setCartData() {

    cart.forEach(item => {
        if (item.count > 0) { // Отображаем только товары с количеством больше нуля
            cartToSend.push(item);
            console.log(cartToSend);
        }

    });
    }

    displayCart();
    const finaleOrder = document.getElementById('order')
    finaleOrder.textContent=order.toFixed(2);
    // Добавляем обработчик события для кнопки "Отправить"
    // sendButton.addEventListener('click', function () {
    //     // Отправьте данные на страницу send.html
    //     localStorage.setItem('cartData', JSON.stringify(cart));
    //     localStorage.setItem('cart', JSON.stringify(cart));
    //     window.location.href = 'send.html';
    // });


    function clearCart() {
        // Очищаем localStorage
        localStorage.removeItem('cartData');
        // Очищаем массив cart
        localStorage.removeItem('cart');
        localStorage.removeItem('order');
        localStorage.removeItem('cartToSend');
    }

    Telegram.WebApp.onEvent("mainButtonClicked", function () {
        setCartData();
        // console.log(cart);
    localStorage.setItem('cartData', JSON.stringify(cart));
    localStorage.setItem('cart', JSON.stringify(cart));
    // Создаем объект для отправки данных
        //console.log(cart.length);
    // Отправляем данные через tg.sendData
    tg.sendData(JSON.stringify(cartToSend));
     clearCart();
    });

    Telegram.WebApp.onEvent("backButtonClicked", function () {
        // window.location.href = 'index';
        window.history.back();
    });
});
