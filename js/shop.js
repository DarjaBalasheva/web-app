let tg = window.Telegram.WebApp;

tg.expand();

tg.MainButton.text = "Просмотреть";
tg.BackButton.show();


document.addEventListener('DOMContentLoaded', function () {
    const type_order = document.getElementsByClassName('type_order')[0].id;
    // console.log(type_order);
    const id_client = document.getElementsByClassName('id_client')[0].id;
    const address_id = document.getElementsByClassName('address_id')[0].id;
    let order = localStorage.getItem('order') || 0;
    // const name_price_list = document.getElementsByClassName('tittle-content')[0].id;
    // console.log(order);

    fetch(`https://fastapisandwich-burgerwhale.amvera.io/${id_client}/${type_order}/${address_id}`) //запрос на список продуктов https://fastapisandwich-burgerwhale.amvera.io
    .then(response => response.json())
    .then((productsList) => {
    const productsDict = productsList;
    localStorage.setItem('min_order', Number(productsDict.min_order));
    const min_order = localStorage.getItem('min_order')||0;
    const products = productsDict.products;
    checkTimeToClearCart();

    const productList = document.getElementById("productList");

    // Объявляем переменные cart и cartItems для хранения выбранных товаров и их количества
    let cart = JSON.parse(localStorage.getItem('cart')) || [];
    let cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
    // Добавьте все товары из products в корзину со значением 0
    products.forEach(product => {
        // Проверяем, существует ли товар с таким именем в корзине
        const existingItem = cart.find(item => item.name_nom === product.name_nom);

        // Если товар отсутствует в корзине, добавляем его
        if (!existingItem) {
            cart.push({id_nomenclature: product.id_nomenclature, name_nom: product.name_nom, count: product.count, price: product.price});
        }
    });
    console.log('--products--')
    console.log(products);
    //Создаём цикл по позициям. Для каждой позици создаем контейнер с информацией, кнопками и функциями
    products.forEach(product => {
        // Создаем элемент товара
        const productItem = document.createElement("div");
        let count = 0
        cart.forEach(item => {
            if (item.name_nom === product.name_nom) {
                count = item.count
            }
        })
        productItem.className = "list";
        productItem.innerHTML = `
            <div class="product">
            
            <img class="products-icon" src="${product.nom_icon}">
            <h2 class="product-name">${product.name_nom}</h2>
            
            <div class="buttons-container">
            <button class="remove-from-cart" id="removeFromCart-${product.id_nomenclature}" data-product="${product.name_nom}">–</button>
            <div id="cartModal-${product.id_nomenclature}" class="cart-modal">
                <span id="cartItemCount-${product.id_nomenclature}" class="cart-item-count">${count}</span>
                <button class="input-count" id="inputCount-${product.id_nomenclature}" data-product="${product.name_nom}"></button>
            </div>
            <button class="add-to-cart" id="addToCart-${product.id_nomenclature}" data-product="${product.name_nom}">+</button>
            </div>
            </div>
            
            <div class="popup" id="popup-${product.id_nomenclature}">
                <div class="popup-body">
                    <div class="popup-content">
                        <button id="popupClose-${product.id_nomenclature}" class="popup-close">X</button>
                        <div class="popup-title">${product.name_nom}</div>
                        <div class="popup-text">Количество</div>
                        <input class="popup-input" id="count-${product.id_nomenclature}" type="number" placeholder="0">
                        <button class="popup-add" id="popupAdd-${product.id_nomenclature}">OK</button>
                    </div>
                </div>
            </div>
        `;

        // Добавляем товар в список
        productList.appendChild(productItem);
        const cartItemCountElement = document.getElementById(`cartItemCount-${product.id_nomenclature}`);

        // Добавляем обработчики событий для кнопки "+"
        const addToCartButton = document.getElementById(`addToCart-${product.id_nomenclature}`);
        addToCartButton.addEventListener('click', function () {
            const productName = addToCartButton.getAttribute('data-product');
            // Добавляем товар и его количество в объект cart
            const existingItem = cart.find(item => item.name_nom === productName);
            existingItem.count++;

            updateCartCount();
            updateCartInStorage();
            updateOrderCount();
            checkMainButton();
        });

        const addCountToCartButton = document.getElementById(`inputCount-${product.id_nomenclature}`);
        addCountToCartButton.addEventListener('click', function () {
            const popup = document.getElementById(`popup-${product.id_nomenclature}`);
            tg.MainButton.hide();
            popup.style.display = "block";
            // window.scroll(top=0,left=0);

        });
        const popopClose = document.getElementById(`popupClose-${product.id_nomenclature}`);
        popopClose.addEventListener('click', function (){
            const popup = document.getElementById(`popup-${product.id_nomenclature}`);
            checkMainButton();
            popup.style.display = "none";
            // window.scroll(top=0,left=0);
            // console.log("Close");
        })
        const popUpAddToCart = document.getElementById(`popupAdd-${product.id_nomenclature}`);
        popUpAddToCart.addEventListener('click', function (){
            var quantity = document.getElementById(`count-${product.id_nomenclature}`).value;
            // console.log(quantity);
                    if (quantity === '' || quantity < 0) {
                        quantity = '0'
                        // console.log(quantity)
                    }
                    if (quantity !== null && !isNaN(quantity)) {
                        // Если пользователь ввел число и не нажал "Отмена", обновляем количество товара
                        const newCount = parseInt(quantity);
                        const productId = product.id_nomenclature;

                        // Обновляем количество товара в корзине
                        const existingItem = cart.find(item => item.id_nomenclature === productId);
                        if (existingItem) {
                            existingItem.count = newCount;
                        }

                        // Обновляем отображение количества товара в модальном окне
                        cartItemCountElement.textContent = newCount;

                        // Обновляем корзину в хранилище
                        updateCartInStorage();
                        updateOrderCount();
                        checkMainButton();
                    }
            const popup = document.getElementById(`popup-${product.id_nomenclature}`);
            popup.style.display = "none";
            checkMainButton();
        });

        const removeFromCartButton = document.getElementById(`removeFromCart-${product.id_nomenclature}`);
        removeFromCartButton.addEventListener('click', function () {
            const productName = removeFromCartButton.getAttribute('data-product');
            // Добавляем товар и его количество в объект cart
            const existingItem = cart.find(item => item.name_nom === productName);
            if (existingItem && existingItem.count > 0) {
                existingItem.count--;
                // Если количество достигло нуля, удаляем товар из корзины
            if (existingItem.count === 0) {
                updateOrderCount();
                const itemIndex = cart.indexOf(existingItem);
            }

            }
            updateCartCount();
            updateCartInStorage();
            updateOrderCount();
            checkMainButton();
        });

    });
    function updateCartInStorage() {
        localStorage.setItem('cart', JSON.stringify(cart));
        activateRemoveButton()
    }

    function checkMainButton() {
        var emptyCart = true;
        cart.forEach(product =>{
            if (order >= Number(min_order)) {
                emptyCart = false;
                tg.MainButton.show();
            }
        })
        if (emptyCart === true) {
            tg.MainButton.hide();
        }
    }
    function activateRemoveButton(){
        cart.forEach(item =>{
            const removeFromCart = document.getElementById(`removeFromCart-${item.id_nomenclature}`);
            if (item.count > 0) {
                removeFromCart.style.backgroundColor = 'var(--bk-corporate-color-third)'
            }
            else {
                removeFromCart.style.backgroundColor = 'var(--bk-corporate-color-second)'
            }
        })

    }
    function updateCartCount() {

    cart.forEach(item => {
        // console.log(item.iiko_code)
        const modalCount = document.getElementById(`cartItemCount-${item.id_nomenclature}`);
        // console.log(modalCount);
         modalCount.textContent = String(item.count);
         activateRemoveButton()
    });
}
    function updateOrderCount(){
        order = 0;
        cart.forEach(item => {
            order += item.count * item.price;
            const finaleOrder = document.getElementById('order')
            finaleOrder.textContent=order.toFixed(2);
        });
    }
    // Вызываем функцию updateCartCount() при загрузке страницы, чтобы обновить количество товаров в корзине
    updateCartCount();
    updateCartInStorage();
    updateOrderCount();
    checkMainButton();
    // console.log(order)
    //Обработчик кнопки main в ТГ
    Telegram.WebApp.onEvent("mainButtonClicked", function () {
        localStorage.setItem('cartData', JSON.stringify(cart));
        localStorage.setItem('cart', JSON.stringify(cart));
        window.location.href = 'cart';
    });

    //Обработчик кнопки back в ТГ
    Telegram.WebApp.onEvent("backButtonClicked", function () {
        clearCart();
        tg.close();
    });

    function clearCart() {
        // Очищаем localStorage
        localStorage.removeItem('cartData');

        // Очищаем массив cart
        localStorage.removeItem('cart');
        localStorage.removeItem('order');
    }
    function checkTimeToClearCart() {
        const currentTime = Date.now();
        let lastTime = localStorage.getItem('lastTime') || null;
        const MutiesForClearCartInMS = 20*60*1000; //30 minuties
        if (lastTime === null || lastTime === '0') {
            localStorage.setItem('lastTime', String(currentTime));
        } else if ((currentTime - Number(lastTime)) > MutiesForClearCartInMS) {
            localStorage.setItem('lastTime', String(currentTime));
            clearCart();
        }
    }
})
});
