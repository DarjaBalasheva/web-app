// send.js
let tg = window.Telegram.WebApp;

tg.expand();
tg.MainButton.show();
tg.MainButton.color = "#88C7DE";
tg.MainButton.text = "Закрыть";
tg.BackButton.hide();

document.addEventListener('DOMContentLoaded', function () {

    // Получите данные из localStorage
    const jsonResult = JSON.parse(localStorage.getItem('cartData'));
    const cart = localStorage.getItem('cart')
    localStorage.removeItem('cart');

    // Отобразите данные в формате JSON
    const jsonData = JSON.stringify(jsonResult, null, 2);
    console.log(jsonData)
    console.log(tg.sendData(jsonResult))
});

Telegram.WebApp.onEvent("mainButtonClicked", function () {
    tg.close()
});