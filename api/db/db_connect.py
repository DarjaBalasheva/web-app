import pandas as pd
import psycopg2 as psycopg2
from psycopg2.extras import DictCursor
from os import environ
from dotenv import load_dotenv

load_dotenv()

db_name = environ['DBNAME_PG']
user = environ['USER_PG']
password = environ['PASSWORD_PG']
host = environ['HOST_PG']
port = environ['PORT_PG']

def query_postgre(query: str):
    conn = psycopg2.connect(dbname=db_name, user=user, password=password, host=host, port=port)
    try:
        with conn:
            with conn.cursor() as cur:
                cur.execute(query)
                if cur.description:
                    full_fetch = cur.fetchall()
                    return full_fetch
    # except as DatabaseError:
    # TODO: exception logg
    except Exception as e:
        print('ошибка БД query_postgre - c запросом:', str, e)
    finally:
        conn.close()


def query_postgre_factory(query: str):
    conn = psycopg2.connect(dbname=db_name, user=user, password=password, host=host, port=port)
    try:
        with conn:
            with conn.cursor(cursor_factory=DictCursor) as cur:
                cur.execute(query)
                if cur.description:
                    full_fetch = cur.fetchall()
                    res = []
                    for row in full_fetch:
                        res.append(dict(row))
                    return res
    # except as DatabaseError:
    # TODO: exception logg
    except Exception as e:
        print('ошибка БД query_postgre - c запросом:', str, e)
    finally:
        conn.close()
