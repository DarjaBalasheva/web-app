from api.db.db_connect import query_postgre_factory


def client_by_id_client(id_client: int) -> list[dict]:
    query = f'''
        SELECT *
        FROM client
        WHERE id_client = {id_client}
        '''
    return query_postgre_factory(query)

def address_by_id(address_id: int) -> list[dict]:
    query = f'''
        SELECT *
        FROM address
        WHERE {address_id} = id_address
        '''
    return query_postgre_factory(query)

def nomenclature_by_id_client(id_client: int) -> list[dict]:
    ''' по названию прайс листа предоставляем позиции к продаже'''

    query = f'''
            SELECT *
            FROM nomenclature_price_list
            INNER JOIN nomenclature USING (id_nomenclature)
            INNER JOIN client on client.price_list = nomenclature_price_list.id_price_list
            WHERE id_client = {id_client}
            ORDER BY name_nom
        '''
    return query_postgre_factory(query)

def get_order_list_by_client_id_and_date(date_order: str, client_id: int, id_address: int):

    sql = f"""SELECT * FROM order_b2b
                INNER JOIN nomenclature USING(id_nomenclature)
                INNER JOIN client_address USING(id_address)
                WHERE id_client = {client_id} AND shipment_day = '{date_order}' AND id_address = {id_address}"""

    return query_postgre_factory(sql)
