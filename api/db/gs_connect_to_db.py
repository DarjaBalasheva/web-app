from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import psycopg2

app = FastAPI()

# Конфигурация подключения к PostgreSQL
db_config = {
    'host': 'your-postgresql-host',
    'port': 5432,
    'database': 'your-database',
    'user': 'your-username',
    'password': 'your-password'
}

class QueryRequest(BaseModel):
    query: str

def execute_query(query):
    connection = None
    try:
        connection = psycopg2.connect(**db_config)
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except (Exception, psycopg2.Error) as error:
        print("Ошибка при выполнении запроса:", error)
        raise HTTPException(status_code=500, detail="Ошибка при выполнении запроса")
    finally:
        if connection:
            connection.close()

@app.post('/query')
def handle_query(request_data: QueryRequest):
    query = request_data.query
    result = execute_query(query)
    return result
