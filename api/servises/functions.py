import datetime
import json
import time
from zoneinfo import ZoneInfo
from api.db import db_request
from fastapi import Request

TIME_ZONE = "Asia/Krasnoyarsk"


def order_is_close():
    time_now = datetime.datetime.now(tz=ZoneInfo(TIME_ZONE))
    time_now = time_now.time()
    close_time = datetime.time(20, 0, 0)
    open_time = datetime.time(7, 0, 0)
    if close_time < time_now or open_time > time_now:
        return True
    else:
        return False


def form_order_to_edit(id_client: int, address_id: int):
    client_id = get_client(id_client=id_client)['id_client']
    current_date = datetime.datetime.now(tz=ZoneInfo(TIME_ZONE))
    day_for_order = current_date + datetime.timedelta(days=1)
    day_for_order = day_for_order.strftime("%Y-%m-%d")
    order_to_edit = db_request.get_order_list_by_client_id_and_date(client_id=client_id,
                                                                        date_order=day_for_order,
                                                                        id_address=address_id)
    all_products = db_request.nomenclature_by_id_client(id_client=int(id_client))
    all_products = add_count_for_product_for_create_order(all_products)
    for product in all_products:
        for product_in_order in order_to_edit:
            if product_in_order["id_nomenclature"] == product["id_nomenclature"]:
                product["count"] = product_in_order["amount"]

    return all_products

def get_data(request: Request, id_client:int, address_id: int, type_order: str) -> dict:
    product = []
    title_text = ""
    client = db_request.client_by_id_client(id_client=int(id_client))[0]
    client_name = client['name_client']
    min_order_amount = client['min_order_amount']

    if type_order == "make_order":
        title_text = "Формирование заказа"
    if type_order == "edit_order":
        title_text = "Редактирование заказа"

    address = db_request.address_by_id(address_id=address_id)[0]
    address_name = address['name_address']

    return {
        "request": request,
        "id_client": id_client,
        "type_order": type_order,
        "title_text": title_text,
        'address_id': address_id,
        "client_name": client_name,
        "min_order_amount": min_order_amount,
        "name_address": address_name,
        }

def get_client(id_client: int):
    client = db_request.client_by_id_client(id_client=int(id_client))[0]
    return client


def add_count_for_product_for_create_order(products_list):
    for product in products_list:
        product['count'] = 0

    return products_list