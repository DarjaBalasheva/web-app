from typing import Union
import json

from fastapi import FastAPI, Request, Query, APIRouter
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from api.db import db_request
from api.servises import functions


router = APIRouter()

router.mount("/scss", StaticFiles(directory="scss/"), name="icons")
router.mount("/js", StaticFiles(directory="js/"), name="js")
templates = Jinja2Templates(directory="templates/")

@router.get("/index", response_class=HTMLResponse)
@router.get("/", response_class=HTMLResponse)
async def home(request: Request,
         client_id: Union[str, None] = Query(default=None),
         user_name: Union[str, None] = Query(default=None),
         date_for_order: Union[str, None] = Query(default=None),
         address_id: Union[str, None] = Query(default=None),
         type_order: Union[str, None] = Query(default=None)
         ):
    is_close = functions.order_is_close()
    if is_close:
        return templates.TemplateResponse(
            "close.html", {"request": request}
        )
    data = f"{user_name} {address_id} {date_for_order}"
    product = db_request.get_products()
    product = json.dumps({"products": product})
    # print(product)
    return templates.TemplateResponse(
        "index.html", {"request": request, "message": data, "products": product}
    )

@router.get("/get_products")
async def get_product():
    product = db_request.get_products()
    min_order = 160
    return {"products": product, "min_order": min_order}


@router.get("/cart", response_class=HTMLResponse)
async def home(request: Request):
    return templates.TemplateResponse(
        "cart.html", {"request": request, "message": "Hello world"}
    )
