from typing import Union
import json

from fastapi import FastAPI, Request, Query, HTTPException
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from api.db import db_request, db_connect
from api.servises import functions
import sentry_sdk

from pydantic import BaseModel
import psycopg2



sentry_sdk.init(
    dsn="https://0cfaf91a28149328cfc45c34b494910f@o4505629002301440.ingest.sentry.io/4505900557205504",
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0,
    # Set profiles_sample_rate to 1.0 to profile 100%
    # of sampled transactions.
    # We recommend adjusting this value in production.
    profiles_sample_rate=1.0,
)


app = FastAPI()
templates = Jinja2Templates(directory="templates/")
app.mount("/js", StaticFiles(directory="js/"), name="js")
app.mount("/scss", StaticFiles(directory="scss/"), name="icons")

@app.get("/index", response_class=HTMLResponse)
@app.get("/", response_class=HTMLResponse)
def home(request: Request,
         type_order: Union[str, None] = Query(default=None),
         id_client: Union[str, None] = Query(default=None),
         address_id: Union[str, None] = Query(default=None),
         ):
    is_close = functions.order_is_close()
    if is_close:
        return templates.TemplateResponse(
            "close.html", {"request": request}
        )

    result = functions.get_data(request=request,
                                id_client=int(id_client),
                                address_id=int(address_id),
                                type_order=type_order)

    return templates.TemplateResponse(
            "index.html", result
        )


@app.get("/{id_client}/{type_order}/{address_id}")
def get_product(id_client:str,
                type_order: str,
                address_id: str):
    product = []
    min_order = functions.get_client(int(id_client))['min_order_amount']

    if type_order == 'make_order':
        product =db_request.nomenclature_by_id_client(id_client=int(id_client))
        product = functions.add_count_for_product_for_create_order(product)
        print(product)
    elif type_order == 'edit_order':
        product = functions.form_order_to_edit(id_client=int(id_client),
                                               address_id=int(address_id))
    return {"products": product, "min_order": min_order}   


@app.get("index/cart", response_class=HTMLResponse)
@app.get("/cart", response_class=HTMLResponse)
def home(request: Request):
    return templates.TemplateResponse(
        "cart.html", {"request": request, "message": "Hello world"}
    )



#Подключение к БД для гугл таблицы
class Item(BaseModel):
    iiko_code: int
    name_nom: str
    short_name: str
    guid_nom: str
    barcode: int
    order_time: str | None = None
    order_1: int
    order_2: int
    id_nomenclature: int
    price: float | None = None
    pack_name: str | None = None
    nom_icon: str | None = None

def execute_query(query):
    connection = None
    try:
        connection = psycopg2.connect(dbname=db_connect.db_name,
                                               user=db_connect.user,
                                               password=db_connect.password,
                                               host=db_connect.host,
                                               port=db_connect.port)
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except (Exception, psycopg2.Error) as error:
        print("Ошибка при выполнении запроса:", error)
        raise HTTPException(status_code=500, detail="Ошибка при выполнении запроса")
    finally:
        if connection:
            connection.close()

@app.get('/nomenclature')
def handle_query():
    query = 'SELECT * FROM nomenclature'
    result = execute_query(query)
    return result

@app.get('/nomenclature/titles')
def handle_query():
    result = []
    query = ("SELECT column_name "
             "FROM information_schema.columns "
             "WHERE table_name = 'nomenclature';")
    titles = execute_query(query)
    for title in titles:
        result.append(title[0])

    return result

@app.post('/nomenclature')
def update_db(items: list[Item]):

    return items
